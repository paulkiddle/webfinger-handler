import { expect, jest, test } from '@jest/globals';
import Webfinger, { AcctUri } from './main';

class Req {
	constructor(user, method = 'GET') {
		this.url = '/.well-known/webfinger?resource=acct:' + user;
		this.method = method;
	}
}

class Res {
	end=jest.fn();
	setHeader=jest.fn();
}

test('webfinger', async()=>{
	const lookup = jest.fn();
	const webfinger = new Webfinger(lookup);

	const req = new Req('e@b.com');
	
	const res = new Res;

	expect(await webfinger.handle(req, res)).toBe(true);

	expect(lookup).toHaveBeenCalledWith(expect.any(AcctUri));
	expect(lookup.mock.calls[0][0].toString()).toEqual('acct:e@b.com');
	expect(res.end.mock.calls).toMatchSnapshot();
});

test('webfinger returning object', async()=>{
	const descriptor = { subject: 'subject', links:[] };
	const lookup = jest.fn(()=>descriptor);
	const webfinger = new Webfinger(lookup);

	const req = new Req('e@b.com');
	
	const res = new Res;

	expect(await webfinger.handle(req, res)).toBe(true);

	expect(lookup).toHaveBeenCalledWith(expect.any(AcctUri));
	expect(lookup.mock.calls[0][0].toString()).toEqual('acct:e@b.com');
	expect(res.end.mock.calls).toMatchSnapshot();
});

test('webfinger invalid domain', async ()=>{
	const lookup = jest.fn();
	const webfinger = new Webfinger(lookup);

	const req = new Req('fake');
	const res = new Res;

	expect(await webfinger.handle(req, res)).toBe(true);

	expect(res.end.mock.calls).toMatchSnapshot();
});


test('webfinger invalid method', async ()=>{
	const lookup = jest.fn();
	const webfinger = new Webfinger(lookup);

	const req = new Req('a@b.com', 'POST');
	const res = new Res;

	expect(await webfinger.handle(req, res)).toBe(true);

	expect(res.end.mock.calls).toMatchSnapshot();
});


test('webfinger invalid url', async ()=>{
	const lookup = jest.fn();
	const webfinger = new Webfinger(lookup);

	const req = {
		url: '/other'
	};
	const res = new Res;

	expect(await webfinger.handle(req, res)).toBe(false);

	expect(res.end).not.toHaveBeenCalled();
});

test('throws error', async ()=>{
	const e = new Error('Test error');
	const lookup = jest.fn(()=>{throw e;});
	const webfinger = new Webfinger(lookup);

	const req = new Req('a@b');
	const res = new Res;

	expect(webfinger.handle(req, res)).rejects.toBe(e);

	expect(res.end).not.toHaveBeenCalled();
});

test('Activitypub-specific handler', async()=>{
	const lookup = jest.fn(() => Webfinger.activitypubResponse('http://example.com/@paul'));
	const webfinger = new Webfinger(lookup);

	const req = new Req('e@b.com');
	
	const res = new Res;

	expect(await webfinger.handle(req, res)).toBe(true);

	expect(lookup).toHaveBeenCalledWith(expect.any(AcctUri));
	expect(lookup.mock.calls[0][0].toString()).toEqual('acct:e@b.com');
	expect(res.end.mock.calls).toMatchSnapshot();
});