# Webfinger Handler

A library that generates a handler for webfinger requests. The created handler works with Node JS HTTP request and response objects, and is otherwise framework agnostic.

```javascript
import WebfingerHandler from 'webfinger-handler';

// Handle requests for `[username]@my-website.example`
// This example is synchronous, but you can make it async if you need to e.g. access a database
const webfinger = new WebfingerHandler(resource => {
    if(resource.host !== 'my-website.example') {
        // Return null if there are no links for this resource
        return null;
    }

    // Either return the whole json-rd response object or just the links array
    return [
        {
            rel: 'http://webfinger.net/rel/profile-page',
            href: `https://my-website.example/profile/${resource.user}`
        },
    ]
});

// This is the standard httpServer.listen callback
export default async function onRequest(req, res) {
    if(await webfinger.handle(req, res)) {
        // The handler will return true if it has handled the request,
        // and false if not (i.e. the req.url path is not '/.well-known/webfinger')
        return;
    }

    // Not a webfinger request - run whatever site code you need to
    res.end('Welcome to my website!');
}
```

## Express usage

For use with Express or Connect, you'll need to wrap your function like so:

```javascript
// Assuming app is your Express instance
app.use(async function webfingerExpress(req, res, next) {
	try {
		if(!await webfinger.handle(req, res)) {
			next();
		}
	} catch(e) {
		next(e);
	}
});
```