# Changelog

## v2.0.1

Set correct main entry in package.json

## v2.0.0

The library now contains Typescript definitions

### Breaking changes from 1.x

The `WebfingerHandler` function is now a class
 - It must be constructed with the `new` keyword
 - It must be called using its `handle(req, res)` method

The `ActivityPubWebfinger` function has been removed
  - Instead, construct a `WebfingerHandler` class and return `Webfinger.activitypubResponse(href)` from the callback
  - Or you can construct your descriptor response manually

The `Acct#toJSON` method has been removed
  - You should not need to use this in regular operation
  - If you need the acct URI, use `acct.uri`